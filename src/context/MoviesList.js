import React, { useState, createContext } from 'react'

export const MoviesList = createContext();

export const MoviesListContainer = (props) => {
    const [movies, setMovies] = useState([
        {
            title: "Harry Potter",
            id: 1,
            price: "$150",
            director: "Mark Thoums",
            body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."

        },
        {
            title: "Inception",
            id: 2,
            price: "$120",
            director: "Carry John",
            body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."

        },
        {
            title: "pirate of the Caribbian",
            id: 3,
            price: "$175",
            director: "James Bond",
            body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."

        },
        {
            title: "Loggan",
            id: 4,
            price: "$190",
            director: "Brillen Yaki",
            body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."

        },
    ])

    const deletemovie = (id) => {

        let newMovies = movies.filter(movie => {
            return movie.id !== id
        })
        setMovies(newMovies);
        console.log("working", movies);
    }

    const addNewMovie=(newmovie)=>
    {
        
        let newListMovie=[...movies,newmovie];
        setMovies(newListMovie);
    }
    return (
        <MoviesList.Provider value={[movies,setMovies, deletemovie,addNewMovie]}>
            {props.children}
        </MoviesList.Provider>
    )
}

