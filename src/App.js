import React from 'react';
import CardContainer from './components/CardContainer'
import { MoviesListContainer } from "./context/MoviesList"
import "./App.css"
import AddMovie from './components/AddMovie';
import Nav from "./components/Nav"
function App() {
  return (
    <div className="App">
      <MoviesListContainer>
        <Nav/>
        <AddMovie />
        <CardContainer />
      </MoviesListContainer>

    </div>
  );
}

export default App;
