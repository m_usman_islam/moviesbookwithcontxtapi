import React, { useContext } from 'react'
import { MoviesList } from "../context/MoviesList"


const Nav = () => {
    const [movies, setMovies, deletemovie] = useContext(MoviesList);
    return (
        <div>
            <div className="nav">
                <p className="logo">Movies Book</p>
                <p>Total Number Movies : {movies.length}</p>
            </div>
        </div>
    )
}

export default Nav
