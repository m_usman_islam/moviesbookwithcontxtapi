import React, { useState, useContext } from 'react'
import { MoviesList } from "../context/MoviesList"
import { v4 as uuidv4 } from "uuid"
const AddMovie = () => {

    const [movies, setMovies, deletemovie, addNewMovie] = useContext(MoviesList);
    const [title, setTitle] = useState("");
    const [price, setPrice] = useState("");
    const [director, setdirector] = useState("");
    const [comments, setComments] = useState("");

    const getMoviesDetail = () => {
        console.log("title", title);
        console.log("price", price);
        console.log("director", director);
        console.log("comments", comments);
        setTitle("");
        setPrice("");
        setdirector("");
        setComments("");
        let movie = {
            id:uuidv4(),
            title,
            price,
            director,
            comments
        }
        addNewMovie(movie)

    }
    return (
        <div>
            <div className="container">
                <h2>Enter Movies Details</h2>
                <h4>(React.js)Using Compnants, Context Api, Props (function & Data passsing Parrent to child and child to parrent ) Functions , Hooks</h4>
                <div className="form">
                    <div className="input-box">
                        <label htmlFor="">Title</label>
                        <div className="input-field ">
                            <input id="last_name" type="text" value={title} className="validate" onChange={(e) => setTitle(e.target.value)} />
                        </div>
                    </div>
                    <div className="input-box">
                        <label htmlFor="">Price</label>
                        <div className="input-field ">
                            <input id="last_name" type="text" value={price} className="validate" onChange={(e) => setPrice(e.target.value)} />
                        </div>
                    </div>
                    <div className="input-box">
                        <label htmlFor="">Director</label>
                        <div className="input-field ">
                            <input id="last_name" type="text" value={director} className="validate" onChange={(e) => setdirector(e.target.value)} />
                        </div>
                    </div>
                    <div className="input-box">
                        <label htmlFor="">Comments</label>
                        <div className="input-field ">
                            <input id="last_name" type="text" value={comments} className="validate" onChange={(e) => setComments(e.target.value)} />
                        </div>
                    </div>
                    <button className="btn waves-effect waves-light custom-btn" onClick={() => getMoviesDetail()}>Add New Movie
                    </button>

                </div>
            </div>

        </div>
    )
}

export default AddMovie
