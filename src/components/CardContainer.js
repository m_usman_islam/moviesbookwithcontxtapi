import React, { useContext } from 'react'
import Card from "./Card"
import { MoviesList } from '../context/MoviesList'
const CardContainer = () => {
    const [movies,setMovies, deletemovie] = useContext(MoviesList);
    return (
        <div className="container">
           
            {movies.reverse().map(movie => {
                return (
                    <Card  id={movie.id}title={movie.title} price={movie.price} director={movie.director} body={movie.body} key={movie.id}  deletemovie={deletemovie} />
                )
            })}
        </div>
    )
}

export default CardContainer
