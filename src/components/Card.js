import React from 'react'

const Card = ({ id, title, price, director, body, deletemovie }) => {
    return (
        <React.Fragment>
            <div className="card">
                <p>{id}</p>
                <h1>{title}</h1>
                <h2>{price}</h2>
                <p>{director}</p>
                <p>{body}</p>
                <button onClick={() => deletemovie(id)} className="waves-effect waves-light btn  red">test</button>
            </div>
        </React.Fragment>
    )
}

export default Card
